<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');

Route::get('/tasks/my_tasks', 'TaskController@my_tasks')->middleware('auth')->name('my_tasks');
Route::resource('/tasks', 'TaskController')->middleware('auth');
Route::get('/tasks/{id}/destroy', 'TaskController@destroy')->middleware('auth')->name('task_delete');
Route::get('/tasks/{id}/done', 'TaskController@done')->middleware('auth')->name('task_done');

Route::get('/home', 'HomeController@index')->name('home');

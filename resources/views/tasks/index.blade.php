@extends('layouts.app')
@section('content')

<h1>This is your tasks list</h1>
@if($all_task)
    <a href = "{{ route('my_tasks') }}"> My Task </a>
@else
    <a href = "{{ route('tasks.index') }}"> All Task </a>
@endif
<table border="4">
    <thead>
        <tr>
            <th>Title</th>
            <th>Edit</th>
            <th>Owner</th>
            @can('admin')
            <th>Delete</th>
            @endcan
            <th>Status</th>
        </tr>
    </thead>   
    <tbody>
        @foreach($tasks as $task)
        <tr>
            
            <td>{{$task->title}}</td>
            
            <td><a href = "{{route('tasks.edit', $task->id)}}"> Edit </a></td>  <!--את החץ נפנה לשם העמודה כמו זאת שנתנו בדאטאבייס -->
            <td>{{$task->user_id}}</td>
            @can('admin')
            <td><a href = "{{route('task_delete', $task->id)}}"> Delete </a></td>
            @endcan
            @if($task->status == 0)
            @can('admin')
                <td><a href = "{{route('task_done', $task->id)}}"> Mark as Done </a></td>
            @else
                <td>Not Done</td>    
            @endcan    
            @else
                <td>Done</td>
            @endif    
        </tr>
        @endforeach
    </tbody>
</table>

<a href = "{{route('tasks.create')}}"> Create a new Task</a>

<script>
       $(document).ready(function(){
       });
   </script>

@endsection